# GENOCHECK

GenoCheck is a web and mobile application that will generate your genetic tree based on your family relations and will come up with the risk prediction of hereditary disease that you or your fututre baby may have. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

You can install GenoCheck from the app store or the google play or visit our web application via the web browser 

## Deployment

To deploy the project on your machine you have to follow these steps:
1. Create a docker-compose.yml file that starts your node-web-app APP and a separate MongoDB instance with a volume mount for data persistence.
2. Run the command 
```
docker-compose up -d
``` 
3. Now you can test the web services  :
_Show the result of risk prediction_
```
POST http://IP_ADDRESS:3001/genocheck/user/predictionFinal
``` 
_Show the genetic tree_
```
POST http://IP_ADDRESS:3001/genocheck/user/predictionFinal
``` 

## How to use

1. Create an account
2. sign in by typing your email  and your password or sign in with google
3. Briefly introduce your parents, grandparents and siblings
4. Include the maximum of the remaining family members along with their hereditary diseases
5. Visit the genetic tree as well as the calculated risk prediction


## Built With

* [NodeJs](https://nodejs.org/en/) 
* [Flutter](https://flutter.dev) 
* [Angular](https://angular.io) 


## Authors

* **El Abed Imen** 
* **Tekaya Dora** 
* **Dhouibi Bilel** 
* **Gabsi Med Salem** 








