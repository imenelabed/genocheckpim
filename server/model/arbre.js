const mongoose = require('mongoose');
//define schema

const Schema= mongoose.Schema;



const arbreSchema = new Schema({
  
  id:String,
  key:Number,
  n:String,
  s:String,
  m:String,
  f:String,
  ux:String,
  a:String,
  niveau:String,
  idfamille:String
  

});

const arbre = mongoose.model('Arbre', arbreSchema);
module.exports= arbre ;
