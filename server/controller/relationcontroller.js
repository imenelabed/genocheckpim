const relation=require('../model/relationmodel')
const user = require('../model/usermodel')
//select
const index = (req,res,next) =>{
    relation.find().then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation '
    })
    })
}
//show
const show = (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.find({iduser:iduser}).then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching with id '
    })
    })
}
//show
const getfamille = (req,res,next) =>{
    let  idfamille=req.params.idfamille
    user.find({idfamille:idfamille}).then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching with id '
    })
    })
}
const showf = (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.find({iduser:iduser}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching with id '
    })
    })
}


//store

const update = (req,res,next) =>{
    let  id=req.body.id
    let  updaterelation={
        iduser: req.body.iduser,
        iduser2: req.body.iduser2,
        type:req.body.type,
       
    }
    relation.findByIdAndUpdate(id,{$set:updaterelation})
      .then(()=>{
          res.json({
              message:'Relation updated'
          })
      })
      .catch(error =>{
          res.json({
              message: 'error'
          })
      })
}



//store

const store = (req,res,next) =>{
    let  relations=new relation({
        iduser: req.body.iduser,
        iduser2: req.body.iduser2,
        type:req.body.type,
       /* maldadie:req.body.maldadie,
        age:req.body.age,
        status:req.body.status */
    })
    relations.save( function (err) {
        if (err) {
            console.log(err)
            res.send("error")
        }
})
}


//delete 
const destroy =(req,res,next)=>{
let id=req.body.id
relation.findByIdAndDelete(id)
.then(()=>{
    res.json({
        message:'deleted successfuly'
    })
})
.catch(error=>{
    res.json({
        message:'error of deleted'
    })
})
}
//mes parents
const getparentconjoint = (req,res,next) =>{
    let  iduser=req.params.iduser
    relation.find({$or: [{ iduser:iduser}, { iduser2: iduser }], $and: [{ $or: [{ type: "pere" }, { type: "mere" },{ type: "conjoint" }] }]},{iduser:1,type:1}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation  '
    })
    })
}
//mes parents
const getmere = (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.findOne({$and:[{iduser:iduser},{type:'mere'}]}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation  '
    })
    })
}
const getpere = (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.findOne({$and:[{iduser:iduser},{type:'pere'}]}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation  '
    })
    })
}


//mes Grand parent
const getGperep = (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.findOne({$and:[{iduser:iduser},{type:'Grandpere_par'}]}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation  '
    })
    })
}
const  getGmerep= (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.findOne({$and:[{iduser:iduser},{type:'Grandmere_par'}]}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation  '
    })
    })
}
const getGperem = (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.findOne({$and:[{iduser:iduser},{type:'Grandpere_mat'}]}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation  '
    })
    })
}
const getGmerem = (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.findOne({$and:[{iduser:iduser},{type:'Grandmere_mat'}]}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation  '
    })
    })
}



//Fréres et Soeurs 
const getfrere = (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.findOne({$and:[{iduser:iduser},{type:'frere'}]}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation  '
    })
    })
}
const getsoeur = (req,res,next) =>{
    let  iduser=req.body.iduser
    relation.findOne({$and:[{iduser:iduser},{type:'soeur'}]}).populate('iduser2').then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error searching relation  '
    })
    })
}

module.exports= {getfamille,getparentconjoint,index,show,store,update,destroy,getmere,getpere,getGmerem,getGperem,getGmerep,getGperep,getfrere,getsoeur,showf}