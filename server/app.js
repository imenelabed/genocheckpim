const authroute=require('./routes/Authroute')
const userroute= require('./routes/user')
const relationroute= require('./routes/relation')
const maladieroute= require('./routes/maladies')
const imageroute=require('./routes/images')
const express = require("express");
const mongoose = require('mongoose')
const morgan= require('morgan')
const cors = require('cors');
const path = require('path')
const compte=require('./model/comptemodel')
//const path =require('path')
const bodyParser = require('body-parser');
const authenticate = require('./middleware/isAuthenticated');
const User = require('./model/usermodel')
//const path =require('path')
const jwt = require('jsonwebtoken')


const passport = require('passport');
const cookieSession = require('cookie-session')
require('./passport-setup');


//Set up detfault mongoose connection
const mongoDb = 'mongodb://mongo/genodb' ;


//const mongoDb = 'mongodb://localhost:27017/genodb' ;
mongoose.connect(mongoDb,{ useNewUrlParser: true}) ;
// Get the default connection
const db =mongoose.connection ;
//bind connection to error event ( to get notification of connection errors
db.on('error',(err)=>{
console.log(err)
})

db.once('open',()=>{
  console.log("connection etabloiie")
  })
  

  const app=express()
  app.use(express.static(path.join(__dirname+'/views')))
  app.use(morgan('dev'))
  app.use(bodyParser.urlencoded({extended:true}))
  app.use(bodyParser.json())
  app.use(cors());
  const PORT=process.env.PORT || 3000
  
  app.listen(PORT,() =>{
    console.log('server running on port ${PORT}')
  })
app.use('/genocheck',authroute)
app.use('/genocheck/user',userroute)
app.use('/genocheck/relation',relationroute)
app.use('/genocheck/maladie',maladieroute)
app.use('/genocheck/image',imageroute)

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');
app.use('/genocheck/arbre/:idfamille',function(req,res){

  res.sendFile(path.join(__dirname,'/GoJS-master/samples/genogram.html'));
//render
});
app.use('/genocheck/arbretwig/:idfamille',function(req,res){
  const { idfamille } = req.params;
  res.render('samples/genogram', { idfamille });

})
app.use('/confirmation/:email',function(req,res){
  let  email=req.params.email
  console.log(email)


compte.findOneAndUpdate({email:email},{ $set:{authentification:"1"}}, function (err, data) {
  if (err) { return (err.message); }
  if(data){
  res.send("Email Acctivated")
  }
  })
});








//=====================================================//



//=====================================================//


app.use(cookieSession({
  name: 'Genocheck',
  keys: ['key1', 'key2']
}))

// Auth middleware that checks if the user is logged in
const isLoggedIn = (req, res, next) => {
  if (req.user) {
      next();
  } else {
      res.sendStatus(401);
  }
}

// Initializes passport and passport sessions
app.use(passport.initialize());
app.use(passport.session());

// Example protected and unprotected routes
app.get('/', (req, res) => res.send('Example Home page!'))
app.get('/failed', (req, res) => res.send('You Failed to log in!'))

// In this route you can see that if the user is logged in u can acess his info in: req.user
app.get('/good', isLoggedIn, (req, res) => res.send(`Welcome mr ${req.user.displayName}!`))

// Auth Routes
app.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

app.get('/google/callback', passport.authenticate('google', { failureRedirect: '/failed' }),
  function (req, res) {
    // Successful authentication, redirect home.
    console.log("req.user", req.user)
    console.log("req.id", req.user.googleId)

    res.redirect('http://localhost:4200/#/auth/googleConnection/' + req.user.googleId);

    // res.status(200).json({result : req.headers.cookie});
  }
);


var corsOptions = {
  origin: 'http://localhost:4200',
  credentials: true,
  optionsSuccessStatus: 200
}
app.get('/getCookies/:id', cors(corsOptions), (req, res) => {
  console.log("req.params.id",req.params)
  console.log("req.googleid",req.user.googleId)
  compte.findOne({ googleId: req.user.googleId })
    .then(user => {
      console.log("user",user);
      if (user) {
        User.findOne({ id: user.id }).then(u => {
          let token = jwt.sign({ firstName: user.firstName, email: user.email, user: u }, 'AzQ,PI)0(', { expiresIn: '1h' })
          let id = user.id
          let idfamille = user.idfamille
          res.json({
            message: 'Login Successful', token, id, idfamille
          })
        });
      } else {
        res.status(401).json({
          message: 'no user found'
        })
      }
    })

})
app.get('/logout', (req, res) => {
  req.session = null;
  req.logout();
  res.redirect('/');
})
