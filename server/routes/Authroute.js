var express = require("express");
var router = express.Router();
const Authcon=require('../controller/Authencontroller')
const path= require('path')
router.post('/',Authcon.login)
router.post('/register',Authcon.register)
router.post('/mailresetpass',Authcon.forgetpassword)
router.post('/updatepass',Authcon.updatepassword)

module.exports=router;