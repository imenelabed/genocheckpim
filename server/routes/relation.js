var express = require("express");
const nodemon = require("nodemon");
var router = express.Router();
const relationcontroller = require('../controller/relationcontroller')
const authenticate = require('../middleware/isAuthenticated');
const relation = require("../model/relationmodel");
const user = require("../model/usermodel");
var bodyParser = require('body-parser');
router.use( bodyParser.json() );  

const arbre = require("../model/arbre");
const { route } = require("./user");
router.get('/', relationcontroller.index)
router.post('/getone', relationcontroller.show)
router.get('/getfamille/:idfamille', relationcontroller.getfamille)
router.post('/myrelation', relationcontroller.showf)
router.post('/addrelation', relationcontroller.store)
router.post('/deleterelation', authenticate, relationcontroller.destroy)
router.post('/updaterelation', relationcontroller.update)
router.post('/getmere', relationcontroller.getmere)
router.post('/getpere', relationcontroller.getpere)
//Grand parents 
router.post('/getGMP', relationcontroller.getGmerep)
router.post('/getGPP', relationcontroller.getGperep)
router.post('/getGMM', relationcontroller.getGmerem)
router.post('/getGPM', relationcontroller.getGperem)
//Frere Soeur
router.post('/getfrere', relationcontroller.getfrere)
router.post('/getsoeur', relationcontroller.getsoeur)
router.get('/getparent/:iduser', relationcontroller.getparentconjoint)
router.get('/getmariage/:idfamille', (req, res) => {
   var idfamille=req.params.idfamille;
    var completed_requests = 0;

   let jsonObject =[];
    relation.find({ $and:[{type: "conjoint" },{idfamille:idfamille}]}, { iduser: 1, iduser2: 1, _id: 0 }, function (err, data) {
        if (err) { return (err.message); }
        //var x=data[0].iduser
        //   console.log("noo"+x)
        data.forEach(function (table) {
            
            
            
            // console.log(table)
            user.find({ $or: [{ id: table.iduser }, { id: table.iduser2 }], $and: [{idfamille:idfamille}] }, { firstName:1, id:1, _id: 0 }, function (err, data1) {
                
                
             

               
                    jsonObject.push({'couple':data1});
                    completed_requests++;
                    if(completed_requests==data.length){
                        res.json({'response':jsonObject})
                    }
             
              }
                
              
            ) 
        
           
            
        })
        
     
     

    }) //.populate('iduser',{firstName:1}).populate('iduser2',{firstName:1});
   
});
router.get('/getmin',(req,res)=>{
    var completed_requestsformin = 0;
    arbre.find({niveau:1}, {id:1, key:1}, function (err, dataz) {
        if (err) { return (err.message); }
        //var x=data[0].iduser
        console.log("the niveau 1"+dataz)
        dataz.forEach(function (table) {
              var x =table.id
              console.log(table.id)
            arbre.findOneAndUpdate({id:x},{ $set: { key:table.key+2}}, function (err, datamin) {
                if(datamin){
                   
                    completed_requestsformin++;
                    if(completed_requestsformin==datamin.length){
                        res.send("succes")
                       
                    }

                }
                if(err){
                    res.send(err)
                }

            })
            
        })
    })
 
        
    
})

// select grand parent 
router.get('/getgrandparents',(req,res)=>{
    var idfamille=req.body.idfamille
    arbre.find({$and:[{idfamille:idfamille},{niveau:3}]} ,{id:1,n:1}).then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error de rechreche membre  '
    })
    })
})



router.get('/getparentarbre',(req,res)=>{
    var iduser=req.body.iduser
    var iduser2=req.body.iduser2


    arbre.findOne({id:iduser},{key:1,n:1},function(err,resu){
if (resu){
    console.log(resu)
    var keyperechild=resu.key
    arbre.findOne({id:iduser2},{key:1,n:1},function(err,resu2){
        if (resu2){
            console.log(resu2)
            var keyperechild=resu2.key
        }
})


    }})
    })


// select grand parent 
router.get('/getgrandparents',(req,res)=>{
    var idfamille=req.body.idfamille
    arbre.find({$and:[{idfamille:idfamille},{niveau:3}]} ,{id:1,n:1}).then(response =>{
        res.json({response
        })
    }) .catch(error=>{
        res.json({ message:'Error de rechreche membre  '
    })
    })
})


    //colorer les grand parent 
router.post('/grandparentcomplex',(req,res)=>{
    let id1=req.body.idg1

    let id2=req.body.idg2
   
                    arbre.findOneAndUpdate({id:id1},{ $set: {a:"D"}},function(err,data){
                        if(data){
                            console.log(data)
                            arbre.findOneAndUpdate({id:id2},{ $set:{a:"D"}},function(err,data1){
                                if(data1){
                                    let relations = new relation({
                                        idfamille:req.body.idfamille,
                                        iduser: id1,
                                        iduser2: id2,
                                        type: "frere"
                                        /* maldadie:req.body.maldadie,
                                         age:req.body.age,
                                         status:req.body.status */
                                    })
                                    relations.save(function (err, resul23) {
                                        if(resul23){
                                    res.json("Success")
                                        }
    
                        })
                    }
    
                    })
    
                }
            })
                }
);
             
     
   

router.post('/getchild', (req, res) => {
let idmere=req.body.idusermere
var completed_requests = 0;
var completed_requestsformin = 0;
var iduser=req.body.iduserpere
var iduser2=req.body.idusermere


arbre.findOne({id:iduser},{key:1,n:1},function(err,resu){
if (resu){
console.log(resu)
var keyperechild=resu.key
arbre.findOne({id:iduser2},{key:1,n:1},function(err,resu2){
    if (resu2){
        console.log(resu2)
        var keymerechild=resu2.key
arbre.findOne({niveau:1},{_id:0,key:1},function(err,data){
    if(data){
       
        console.log("heey min",data.key)
        var minid=data.key
        var minplusone=data.key+1
user.findOne({id:idmere},{_id:0,niveau:1},function (err, data) {
    if(data){
var n=data.niveau

    let adduser = new user({

        firstName: req.body.firstName,
        gender: req.body.gender,
        maladie: req.body.maladie,
        idfamille: req.body.idfamille,
        niveau:n-1


    })
    adduser.save(function (err, resu) {
        if (resu) {
            
            let idchild = resu.id;
            let relations = new relation({
                idfamille:req.body.idfamille,
                iduser: req.body.idusermere,
                iduser2: idchild,
                type: "mere"
                /* maldadie:req.body.maldadie,
                 age:req.body.age,
                 status:req.body.status */
            })
            relations.save(function (err, resul2) {
                if (resul2) {
                    let relations = new relation({
                        idfamille:req.body.idfamille,
                        iduser: req.body.iduserpere,
                        iduser2: idchild,
                        type: "pere"
                        /* maldadie:req.body.maldadie,
                         age:req.body.age,
                         status:req.body.status */
                    })
                    relations.save(function (err, resul23) {
                        if (resul23) {
                            let partenaire=req.body.firstNamepartenaire
                            if(partenaire!=null){
                            let adduser = new user({

                                firstName: req.body.firstNamepartenaire,
                                gender: req.body.genderpatenaire,
                                maladie: req.body.maladiepartenaire,
                                niveau:n-1,
                                idfamille: req.body.idfamille,
                                
                        
                        
                            })
                            adduser.save(function (err, resupartenaire) {
                                if (resupartenaire) {
                                    let idpartenaire = resupartenaire.id;
                    let relations = new relation({
                        idfamille:req.body.idfamille,
                        iduser: idpartenaire,
                        iduser2: idchild,
                        type: "conjoint"
                        /* maldadie:req.body.maldadie,
                         age:req.body.age,
                         status:req.body.status */
                    })
                    relations.save(function (err, t) {
                        if (t) { 
                           
                            if(req.body.maladie==true){
                                var mx ="S"
                            }  
                                        let addarb = new arbre({
                                            id:idchild,
                                            key: minid,
                                            n: req.body.firstName,
                                            s: req.body.gender,
                                            m: keymerechild,
                                            f: keyperechild,
                                            niveau:n-1,
                                            a: mx,
                                            ux:minplusone,
                                            idfamille:req.body.idfamille,
                                          
            
            
                                        })
                                        addarb.save(function (err, childresu) {
                                            if (childresu) {
                                                if(req.body.maladiepartenaire==true){
                                                    var px ="S"
                                                }  
                                                let addarb = new arbre({

                                                    key: minplusone,
                                                    id:idpartenaire,
                                                    n: req.body.firstNamepartenaire,
                                                    s: req.body.genderpatenaire,
                                                    f:req.body.iduserperepar,
                                                    m:req.body.idusermerepar,
                                                    
                                                    niveau:n-1,
                                                    a:px,
                                                    idfamille:req.body.idfamille,
                                                   
                                                   
                                                  
                    
                    
                                                })
                                                addarb.save(function (err, childresu) {
                                                    if (childresu) {
                                                        arbre.find({$and:[{ m: req.body.idusermere}, { key: { $ne: idchild } }]}, { key:1}, function (err, data) {
                                                            if (err) { return (err.message); }
                                                            //var x=data[0].iduser
                                                            console.log("noo"+data)
                                                            data.forEach(function (table) {
                                                                
                                                                let relations = new relation({
                                                    
                                                                    iduser: table.key,
                                                                    iduser2: idchild,
                                                                    type: "Frere"
                                                                    /* maldadie:req.body.maldadie,
                                                                     age:req.body.age,
                                                                     status:req.body.status */
                                                                })
                                                                relations.save(function (err, t) {
                                                                    if (t) {
                                                        
                                                                        completed_requests++;
                                                                        if(completed_requests==data.length){
                                                                            console.log('Child Added and relation Updated with partner')
                                                                            arbre.find({niveau:1}, {id:1, key:1}, function (err, dataz) {
                                                                                if (err) { return (err.message); }
                                                                                //var x=data[0].iduser
                                                                                console.log("the niveau 1"+dataz)
                                                                                dataz.forEach(function (table) {
                                                                                      var x =table.id
                                                                                      console.log(table.id)
                                                                                    arbre.findOneAndUpdate({id:x},{ $set: { key:table.key+2}}, function (err, datamin) {
                                                                                        if(datamin){
                                                                                           
                                                                                            completed_requestsformin++;
                                                                                            if(completed_requestsformin==datamin.length){
                                                                                                res.json("succes")
                                                                                               
                                                                                            }
                                                                        
                                                                                        }
                                                                                        if(err){
                                                                                            res.send(err)
                                                                                        }
                                                                        
                                                                                    })
                                                                                    
                                                                                })
                                                                            })
                                                                         
                                                                        }
                                                      
                    
                                                 
                                                }})
                                              
            
                                         
                                        })
                               
                               
                               
                                
                            })
                    
                        }})
                    }})
                }         
                })
                        } 
                    })
                        }else{
                            if(req.body.maladie==true){
                                var x ="S"
                            }  
                            console.log(x)
                                            let addarb = new arbre({
    
                                                id: idchild,
                                                key:minid,
                                                n: req.body.firstName,
                                                s: req.body.gender,
                                                m: keymerechild,
                                                f: keyperechild,
                                                a: x,
                                                niveau:n-1,
                                                idfamille:req.body.idfamille,
                                               
                                              
                
                
                                            })
                                            addarb.save(function (err, childresu) {
                                                if(childresu){
                                                    arbre.find({$and:[{ m: keymerechild}, { key: { $ne: idchild } }]}, { key:1}, function (err, data) {
                                                        if (err) { return (err.message); }
                                                        //var x=data[0].iduser
                                                        console.log(data)
                                                        data.forEach(function (table) {
                                                           
                                                            if(table.key!=idchild){
                                                            let relations = new relation({
                                                
                                                                iduser: table.key,
                                                                iduser2: idchild,
                                                                type: "Frere"
                                                                /* maldadie:req.body.maldadie,
                                                                 age:req.body.age,
                                                                 status:req.body.status */
                                                            })
                                                            relations.save(function (err, t) {
                                                                if (t) {
                                                                    completed_requests++;
                                                                    if(completed_requests==data.length){
                                                                        console.log('Child Added and relation Updated')



                                                                        arbre.find({niveau:1}, {id:1, key:1}, function (err, dataz) {
                                                                            if (err) { return (err.message); }
                                                                            //var x=data[0].iduser
                                                                            console.log("the niveau 1"+dataz)
                                                                            dataz.forEach(function (table) {
                                                                                  var x =table.id
                                                                                  console.log(table.id)
                                                                                arbre.findOneAndUpdate({id:x},{ $set: { key:table.key+1}}, function (err, datamin) {
                                                                                    if(datamin){
                                                                                       console.log(datamin)
                                                                                        completed_requestsformin++;
                                                                                        if(completed_requestsformin==datamin.length){
                                                                                            res.json("succes")
                                                                                           
                                                                                        }
                                                                    
                                                                                    }
                                                                                    if(err){
                                                                                        res.send("try again ")
                                                                                    }else{
                                                                                        res.send("Addded")
                                                                                    }
                                                                    
                                                                                })
                                                                                
                                                                            })
                                                                        })
                                                                    }
                                                }
                                            })
                                        }
                                        })
                                    })
                                }})
                            }
                        }
                        })
                    }
                })
            }
        })
    }
        })//here


    }
}

).sort('key').limit(1)//hereee

}
})


    }})
    

})





    


                    
               
        
    
router.get('/postrelations', (req, res) => {
   
    let iduser= req.body.idusermere
    arbre.find({ m: iduser }, { key:1}, function (err, data) {
        if (err) { return (err.message); }
        //var x=data[0].iduser
        console.log("noo"+data)
        data.forEach(function (table) {
            console.log(table.key)
            let relations = new relation({

                iduser: table.key,
                iduser2: req.body.frereid,
                type: "Frere"
                /* maldadie:req.body.maldadie,
                 age:req.body.age,
                 status:req.body.status */
            })
            relations.save(function (err, t) {
                if (t) {
                
            
           
           
           
            
        }})

    })
})
})



module.exports = router;
