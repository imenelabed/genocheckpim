const multer  = require('multer');
var express = require("express");
const path = require('path');
var fs = require('fs');
var router = express.Router();
const user = require("../model/usermodel");
var bodyParser = require('body-parser');
router.use( bodyParser.json() );  
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads');
    },
    filename: (req, file, cb) => {
        console.log(file);
        cb(null,(file.originalname));
    }
});
const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png' || file.mimetype== 'image/jpg') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}
const upload = multer({ storage: storage, fileFilter: fileFilter });


/*
router.post('/uploadd', upload.single('upload'), (req, res, next) => {
    
  try {
      return res.status(201).json({
          message: 'File uploded successfully'
          
      });

    
  } catch (error) {
      console.error(error);
  }
});*/
// Avoir acces a uploads folder
  router.use('/uploads', express.static('uploads'));


  //11--stocker les images a uplaods
  router.post(
    '/upload',
    multer({
      storage: storage
    }).single('uploads'), function(req, res) {
     res.send(req.file.originalname)
    
      
      return res.status(200).end();
    });
 //22--get images from file uploads
 /*
  router.get('/uploads/:upload', function (req, res){
    file = req.params.upload;
    console.log(__dirname)
    console.log(req.params.upload);
    var img = fs.readFileSync(__dirname + "/uploads/" + file);
    res.writeHead(200, {'Content-Type': 'image/png' });
    res.end(img, 'binary');
  
  });
*/
//12--stocker le nom de l'image dans la BD
  router.get('/updateimage/:image/:id' , (req, res) =>{
    let image=req.params.image
    let id=req.params.id
    console.log(id)
    user.findOneAndUpdate({id:id},{ $set:{image:image}}, function (err, data) {
        if (err) { return (err.message); }
        if(data){
        res.send("image Updated")
        }
        })
    
      })

//21--get le Nom de l'image de cet utilisateur
router.get('/selctimage/:id' , (req, res) =>{
    
        let id=req.params.id
    user.findOne({id:id},{_id:0,image:1}, function (err, data) {
        if (err) { return (err.message); }
        if(data){
        res.send(data)
        }
        })
    
      })

        
        
        

  
module.exports=router;