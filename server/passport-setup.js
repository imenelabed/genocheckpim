const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const User = require('./model/usermodel')
const Compte = require('./model/comptemodel')
const random = require('random')

passport.serializeUser(function (user, done) {
  /*
  From the user take just the id (to minimize the cookie size) and just pass the id of the user
  to the done callback
  PS: You dont have to do it like this its just usually done like this
  */
  console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
  console.log(user)
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  /*
  Instead of user this function usually recives the id 
  then you use the id to select the user from the db and pass the user obj to the done callback
  PS: You can later access this data in any routes in: req.user
  */
  done(null, user);
});

passport.use(new GoogleStrategy({
  clientID: "517335373108-pntf5hhv6d4vl7bthlt1qmfilkmob75o.apps.googleusercontent.com",
  clientSecret: "0tSLd5gueFBlGIT_nkEPgHHO",

  callbackURL: "http://localhost:3000/google/callback"
}, async (accessToken, refreshToken, profile, done) => {
  //get the user data from google 
  let email = profile.emails[0].value;
  let nom = profile.name.givenName;
  let aaa = "aaaaaaaaaaaaaaaa";
  let prenom = profile.name.familyName;
  let idfamille = nom[1] + prenom[2] + nom[2] + prenom[1] + random.int(0, 100);
  console.log("profile", profile)
  let newCompte = new Compte({
    googleId: profile.id,
    firstName: profile.familyName,
    aa: aaa,
    email: profile.emails[0].value,
    idfamille: idfamille,
    id: null
  })

  try {
    //find the user in our database  105873331821990525804
    let user = await Compte.findOne({ googleId: profile.id })
    if (user) {
      //If user present in our database.
      done(null, user)
    } else {

      let user = new User({
        firstName: profile.name.givenName,
        lastName: profile.name.familyName,
        gender: '',
        date_birth: '',
        idfamille: idfamille
      })
      user.save().then(data => {
        newCompte.id = data.id;
        newCompte.save().then(success => {
          done(null, user)
        })
      })
    }
  } catch (err) {
    console.error(err)
  }
}))

// used to serialize the user for the session
passport.serializeUser((user, done) => {
  done(null, user.id)
})

// used to deserialize the user
passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => done(err, user))
})
